package az.mastersoft.natiq;

import java.io.Serializable;

/**
 *
 * @author Natiq
 */

public class AdminPojo implements Serializable{
    private int id;
    private String username;
    private String password;
    private String email;

    public AdminPojo() {
    }

    public AdminPojo(int id, String username, String password,String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    
    
    public void setEmail(String email) {
        this.email = email ;
    }
    
    public String getEmail(){
        return email;
    }
}
