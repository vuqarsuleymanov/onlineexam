/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.mastersoft.natiq;


import az.mastersoft.rufan.DBO;
import az.mastersoft.rufan.UsersPojo;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author rqf
 */
@WebServlet(name = "Upload", urlPatterns = {"/upload"})
public class UploadServlet extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        doPost(request, response);
    }

   

    /**
     * uploadFile method used to upload file to server.
     *
     **
     * @param request
     * @param response
     * @throws java.io.IOException
     */
   

    @Override
    
      
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html;charset=UTF-8");

        request.setCharacterEncoding("UTF-8");
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        
        try {
            List<FileItem> fields = upload.parseRequest(request);

            Iterator<FileItem> it = fields.iterator();
            if (!it.hasNext()) {
                // fayl yoxdur mesaj
                return;
            }
            HttpSession session = request.getSession();
            
            String image="";
            String absoluteDiskPath="";
            int notImg = 0 ; 
            
            while (it.hasNext()) {
                ServletContext servletContext = request.getSession().getServletContext();
                FileItem fileItem = it.next();
               
                    if(fileItem.getFieldName().equals("file")){
                        
                        System.out.println("Burda");
                        image = fileItem.getName();
                        String path = image.substring(image.lastIndexOf("."));
                        image = DBO.GenerateUserCode()+ image;
                        
                        System.out.println(path);
                        
                          if (     path.equals(".PNG") || path.equals(".png")
                                || path.equals(".JPG") || path.equals(".jpg")
                                || path.equals(".GIF") || path.equals(".gif")) {

                            String relativeWebPath = "/images/profile";
                            absoluteDiskPath = servletContext.getRealPath(relativeWebPath);
                            File file = new File(absoluteDiskPath + "/", image);
                            fileItem.write(file);

                        } else {
                            notImg = 1;
                            request.setAttribute("imgError", "error");
                            response.sendRedirect("profile.jsp");                           
                        }
                    }
     
            }
     
            // image olub olmadigni yoxladiqdan sonra emelleri yerine yetirir
            if (notImg == 0) {
                String id = session.getAttribute("user_id").toString();
                System.out.println("id = " + id);
                DBOperations.changeImg(Integer.parseInt(id), "images/profile/" + image);
                response.sendRedirect("profile.jsp");
            } else {
                response.sendRedirect("profile.jsp");
            }
              
        } catch (FileUploadException e) {
            e.printStackTrace(System.out);
        } catch (Exception ex) {
            Logger.getLogger(UploadServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("Hi");
    }
    
    }