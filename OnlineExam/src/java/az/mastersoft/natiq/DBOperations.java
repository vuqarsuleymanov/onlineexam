/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.mastersoft.natiq;

import az.mastersoft.rufan.DBO;
import static az.mastersoft.rufan.DBO.close;
import static az.mastersoft.rufan.DBO.connected;
import az.mastersoft.rufan.UsersPojo;
import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DBOperations {
    private static Connection conn = null;
    private static ResultSet rs = null;
    private static PreparedStatement pst = null;
    
    public static void connected(){
        try {
            DriverManager.registerDriver(new Driver());
            conn= DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/exam?characterEncoding=UTF-8", "vuqar", "admin");
        } catch (SQLException ex) {
            Logger.getLogger(DBOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void close(){
        try {
            pst.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void update(UsersPojo p){
        try {          
            connected(); 
            String sql="update exam.users set firstname='"+p.getFirstname()+"',lastname='"+p.getLastname()+"',phone='"+p.getPhone()+"',gender='"+p.getGender()+"',zone='"+p.getZone()+"'where email='"+p.getEmail()+"'";
            pst=conn.prepareStatement(sql); 
            pst.executeUpdate(); 
            
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);   
        }finally{
            close();
        } 
   }
    
    public static String getPassword(AdminPojo p){
        try {
            String sql = "select password from users where email='" + p.getEmail() + "'";
            connected();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            if(rs.next())
                return rs.getString(1);
            else
                return null;
        } catch (SQLException ex) {
            Logger.getLogger(DBOperations.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }finally{
            close();
        }
    }
    
    public static UsersPojo getInfo(int id){
        try {
            String sql = "select firstname,lastname,email,phone,zone,gender,birth_date, photo from users where id=" + id;
            connected();
             UsersPojo up = null;
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            if(rs.next()){
                up = new UsersPojo();
                up.setFirstname(rs.getString(1));
                up.setLastname(rs.getString(2));
                up.setEmail(rs.getString(3));
                up.setPhone(rs.getString(4));
                up.setZone(rs.getString(5));
                up.setGender(rs.getString(6));
                up.setBirthDate(rs.getString(7));
                up.setPhoto(rs.getString(8));
                
            }
            return up;
        } catch (SQLException ex) {
            Logger.getLogger(DBOperations.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }finally{
            close();
        }
    }
  
    public static void changeImg(int id, String path) {
        try {
            String sql = "update users set photo='" + path + "'" + "where id=" + id ;
            connected();
            pst = conn.prepareStatement(sql);
            pst.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace(System.out);
        }finally{
            close();
        }
    }

}


