/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.mastersoft.murad;

import az.mastersoft.rufan.DBO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rufan
 */
public class DBOperations {
     private static Connection conn = null;
    private static  ResultSet rs=null;
    private static PreparedStatement ps = null;              
    public static void connected(){
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn=DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/exam","vuqar","admin");
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public static void close(){
        try {
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
