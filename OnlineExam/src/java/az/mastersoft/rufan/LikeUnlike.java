/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.mastersoft.rufan;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Rufan
 */
@WebServlet(name = "LikeUnlike", urlPatterns = {"/LikeUnlike"})
public class LikeUnlike extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        
         if(request.getParameter("like")!=null){
             if(DBO.check_like_topic(Integer.parseInt(session.getAttribute("user_id").toString()),Integer.parseInt(request.getParameter("topic")))==false){  
             DBO.increase_like_topic(Integer.parseInt(session.getAttribute("user_id").toString()), Integer.parseInt(request.getParameter("topic")), 1);
             }
             else if(DBO.check_like_topic(Integer.parseInt(session.getAttribute("user_id").toString()),Integer.parseInt(request.getParameter("topic")))){
                 DBO.update_like_type_topic(Integer.parseInt(session.getAttribute("user_id").toString()),Integer.parseInt(request.getParameter("topic")),1);
             }
         }
         else if(request.getParameter("unlike")!=null){
             if(DBO.check_like_topic(Integer.parseInt(session.getAttribute("user_id").toString()),Integer.parseInt(request.getParameter("topic")))==false){  
             DBO.increase_like_topic(Integer.parseInt(session.getAttribute("user_id").toString()), Integer.parseInt(request.getParameter("topic")), 0);
             }
             else if(DBO.check_like_topic(Integer.parseInt(session.getAttribute("user_id").toString()),Integer.parseInt(request.getParameter("topic")))){
                 DBO.update_like_type_topic(Integer.parseInt(session.getAttribute("user_id").toString()),Integer.parseInt(request.getParameter("topic")),0);
             }
         }
         
         response.sendRedirect("forumDetails.jsp?id="+request.getParameter("topic"));
        
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
