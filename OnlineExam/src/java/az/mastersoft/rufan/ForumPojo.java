/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.mastersoft.rufan;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 *
 * @author Rufan
 */
public class ForumPojo implements Serializable{
    private int id;
    private String title;
    private String content;
    private int fk_user;
    private String insertDate;
    private int like;
    private int unlike;
    private int like_type;
   

    public void setLike_type(int like_type) {
        this.like_type = like_type;
    }

    public int getLike_type() {
        return like_type;
    }


    public void setLike(int like) {
        this.like = like;
    }

    public void setUnlike(int unlike) {
        this.unlike = unlike;
    }

    public int getLike() {
        return like;
    }

    public int getUnlike() {
        return unlike;
    }

    public ForumPojo(int id, String title, String content, int fk_user, String insertDate,int like, int unlike) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.fk_user = fk_user;
        this.insertDate = insertDate;
        this.like = like;
        this.unlike = unlike;
        
    }

    public ForumPojo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getFk_user() {
        return fk_user;
    }

    public void setFk_user(int fk_user) {
        this.fk_user = fk_user;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

   
}
