/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.mastersoft.rufan;

import java.io.Serializable;

/**
 *
 * @author Rufan
 */
public class CommentPojo implements Serializable{
    private int id;
    private String comment;
    private int fk_topic;
    private int fk_user;
    private String insert_date;
    private int like;
    private int unlike;

    public void setLike(int like) {
        this.like = like;
    }

    public void setUnlike(int unlike) {
        this.unlike = unlike;
    }

    public int getLike() {
        return like;
    }

    public int getUnlike() {
        return unlike;
    }

    public CommentPojo() {
    }
    
    

    public CommentPojo(int id, String comment, int fk_topic, int fk_user, String insert_date,int like, int unlike) {
        this.id = id;
        this.comment = comment;
        this.fk_topic = fk_topic;
        this.fk_user = fk_user;
        this.insert_date = insert_date;
        this.like=like;
        this.unlike=unlike;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(String insert_date) {
        this.insert_date = insert_date;
    }

    
    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setFk_topic(int fk_topic) {
        this.fk_topic = fk_topic;
    }

    public void setFk_user(int fk_user) {
        this.fk_user = fk_user;
    }

    public int getId() {
        return id;
    }

    public String getComment() {
        return comment;
    }

    public int getFk_topic() {
        return fk_topic;
    }

    public int getFk_user() {
        return fk_user;
    }

   
}
