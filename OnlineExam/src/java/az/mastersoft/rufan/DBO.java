/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.mastersoft.rufan;

import az.mastersoft.javanshir.DAO;
import static com.sun.corba.se.impl.util.Utility.printStackTrace;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rufan
 */

public class DBO {

    private static Connection conn = null;
    private static ResultSet rs = null;
    private static PreparedStatement ps = null;

    public static void connected() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/exam?characterEncoding=UTF-8", "vuqar", "admin");
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void close() {
        try {
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean checkUser(UsersPojo p) {
        try {
            connected();
            String sql = "select * from exam.users where email='" + p.getEmail() + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            close();

        }
    }

    public static String GenerateUserCode() {
        try {
            String sql = "select substring(MD5(RAND()) from 1 for 10) as code";
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            } else {
                return null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            close();
        }

    }

    public static ArrayList<UsersPojo> zone() {

        ArrayList<UsersPojo> list = new ArrayList<>();
        try {
            String sql = "select * from exam.zone order by name asc";
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                UsersPojo p = new UsersPojo();

                p.setZone(rs.getString(2));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            close();
        }
    }
    
    public static ArrayList<UsersPojo> univercity() {      

        ArrayList<UsersPojo> list = new ArrayList<>();
        try {
            String sql = "select * from exam.univercity order by name ASC";
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                UsersPojo p = new UsersPojo();

                p.setUnivercity(rs.getString(2));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            close();
        }
    }

   
     public static String date_format(String input) {
        try {
            if (input == null) {
                return null;
            }

            DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            java.util.Date date = inputFormatter.parse(input);

            DateFormat outputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String output = outputFormatter.format(date);
            return output;
        } catch (ParseException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
                 return null;
        }
   
    }
    
     public static ForumPojo getForumById(int id) {

        ForumPojo p = new ForumPojo();
        try {
            String sql = "select * from exam.topics where id="+id;
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                

                p.setId(rs.getInt(1));
                p.setTitle(rs.getString(2));
                p.setContent(rs.getString(3));
                p.setFk_user(rs.getInt(4));
                p.setInsertDate(date_format(rs.getString(5)));
         

                
            }

            return p;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            close();
        }
    }
     
     public static UsersPojo getUserById(int id) {

        UsersPojo p = new UsersPojo();
        try {
            String sql = "select id, firstname, lastname, photo from exam.users where id="+id;
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                

                p.setId(rs.getInt(1));
                p.setFirstname(rs.getString(2));
                p.setLastname(rs.getString(3));
                p.setPhoto(rs.getString(4));

                
            }

            return p;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            close();
        }
    }
     public static ForumPojo TopicLike(int id){
          ForumPojo fp = new ForumPojo();
        try {
            String sql="select like_type from exam.topic_like where fk_topic='"+id+"'";
            connected();
            ps=conn.prepareStatement(sql);
            rs=ps.executeQuery();
            
            if(rs.next()){
      
                fp.setLike(rs.getInt(1));
         
            }
            return fp;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        finally{
            close();
        }
         
     }
    
     
     public static int view(int id){
        try {
            int view_count = 0;
            String sql="select view_count from exam.topics where id='"+id+"'";
            connected();
            ps=conn.prepareStatement(sql);
            rs=ps.executeQuery();
            if(rs.next()){
                view_count=rs.getInt(1);
            }
            return view_count;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        finally{
            close();
        }
     
     }
     public static void insert_view_count(int id,int count){
        try {
            String sql="update  exam.topics set view_count='"+count+"' where id='"+id+"' ";
            connected();
            ps=conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            close();
        }
         
     }
      public static void update_like_type_topic(int fk_user,int fk_topic,int type){
        try {
            String sql="update  topic_like set like_type="+type+" where fk_topic="+fk_topic+" and fk_user="+fk_user+" ";
            connected();
            ps=conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            close();
        }
         
     }
     public static int getLikeCountByFkTopic(int fk_topic, int like_type){
        try{ 
         int like=0;   
         String sql="select count(*) from exam.topic_like where fk_topic="+fk_topic+" and like_type ="+like_type;
         connected();
         ps=conn.prepareStatement(sql);
         rs=ps.executeQuery();
         if(rs.next()){
             like=rs.getInt(1);
         }
         return like;
        }catch(SQLException ex){
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        finally{
            close();
        }
        
     }
     public static void increase_like_topic(int fk_user,int fk_topic, int liketype){
        try {
            String sql="insert into topic_like values(0, "+liketype+", "+fk_topic+", "+fk_user+" )"; 
            connected();
            ps=conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            close();
        }
     }
     public static boolean check_like_topic (int fk_user, int fk_topic){
         
        try {
            String sql= "select * from exam.topic_like where fk_user="+fk_user+" and fk_topic="+fk_topic+"";
            connected();
            ps=conn.prepareStatement(sql);
            rs=ps.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
         
         finally{
            close();
        }
     }
     
     
     
      public static int getLikeCountByFkComment(int fk_comment, int like_type){
        try{ 
         int like=0;
         String sql="select count(*) from exam.comment_like where fk_comment="+fk_comment+" and like_type ="+like_type;
         connected();
         ps=conn.prepareStatement(sql);
         rs=ps.executeQuery();
         if(rs.next()){
             like=rs.getInt(1);
         }
         return like;
        }catch(SQLException ex){
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        finally{
            close();
        }
        
     }
      
       public static void increase_like_comment(int fk_user,int fk_comment, int liketype){
        try {
            String sql="insert into comment_like values(0, "+liketype+", "+fk_comment+", "+fk_user+" )";
            connected();
            ps=conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            close();
        }
     }
       
       
        public static boolean check_like_comment (int fk_user, int fk_comment){
         
        try {
            String sql= "select * from exam.comment_like where fk_user="+fk_user+" and fk_comment="+fk_comment+"";
            connected();
            ps=conn.prepareStatement(sql);
            rs=ps.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
         
          finally{
            close();
        }
     }
      public static void update_like_type_comment(int fk_user,int fk_comment,int type){
        try {
            String sql="update  comment_like set like_type="+type+" where fk_comment="+fk_comment+" and fk_user="+fk_user+" ";
       
            connected();
            ps=conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            close();
        }
         
     }
        public static List<ForumPojo> Topics(int start) {
        try {
            List<ForumPojo> list = new ArrayList<>();
            String sql = "select * from exam.topics LIMIT " + start + ",  " + 10;
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
              ForumPojo p = new ForumPojo();

                p.setId(rs.getInt(1));
                p.setTitle(rs.getString(2));
                p.setContent(rs.getString(3));
                p.setFk_user(rs.getInt(4)); 
                p.setInsertDate(date_format(rs.getString(5)));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }
         public static int getCountOfTopics() {
        try {
            connected();
            int count = 0;
            String sql = "select count(*) from topics";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            if (count % 10 == 0) {
                return count / 10;
            } else {
                return count / 10 + 1;
                
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } finally {
            close();
        }

    }

        
      public static ArrayList<CommentPojo> comments(int id,int start) {

        ArrayList<CommentPojo> list = new ArrayList<>();
        try {
            String sql = "select * from exam.comments where exam.comments.fk_topics="+id+" LIMIT "+ start + ", "+10+" ";
            connected();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                CommentPojo p = new CommentPojo();

                p.setId(rs.getInt(1));
                p.setComment(rs.getString(2));
                p.setFk_user(rs.getInt(4));
                p.setFk_topic(rs.getInt(3));
                p.setInsert_date(date_format(rs.getString(5)));
                
                list.add(p);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            close();
        }
    }
      
      
         public static int getCountOfComments() {
        try {
            connected();
            int count = 0;
            String sql = "select count(*) from comments";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            if (count % 10 == 0) {
                return count / 10;
            } else {
                return count / 10 + 1;
                
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } finally {
            close();
        }

    }
         
         public static int getCountOfComments2(int fk_topics) {
        try {
            connected();
            int count = 0;
            String sql = "select count(*) from comments where fk_topics="+fk_topics+"";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }   
            return count;
            

        } catch (SQLException ex) {
            Logger.getLogger(DBO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } finally {
            close();
        }

    }
         
  
    public static void main(String[] args) throws IOException {

      int count=getCountOfComments2(5);
        System.out.println(count);
    }
     

           
           /*    public static void send() throws FileNotFoundException, IOException{
                                 BufferedReader br = null;
                                try {                                      
                                String s;
                                br = new BufferedReader(new FileReader("C:\\Users\\Rufan\\Desktop\\univer.txt"));                          
                                 while ((s = br.readLine()) != null) {
                                    
                                 System.out.println(s);
                                     insert(s);                                  
                                 }

                                } catch (IOException e) {
                                 e.printStackTrace();
                                 } finally {
                                 {
                                     br.close();
                                 }
                                 }
                   
                } */
                 
   public static void insert(UsersPojo p){
        try {
            
            connected(); 
             String sql="insert into exam.users(firstname,lastname,middlename,phone,email,password,usercode,gender,birth_date,zone,univercity,active,photo) values ('"+p.getFirstname()+"','"+p.getLastname()+"','"+p.getMiddlename()+"','"+p.getPhone()+"','"+p.getEmail()+"','"+p.getPassword()+"','"+p.getUsercode()+"','"+p.getGender()+"','"+p.getBirthDate()+"','"+p.getZone()+"','"+p.getUnivercity()+"','"+p.getActive()+"','"+p.getPhoto()+"')"; 
           
              
                   ps=conn.prepareStatement(sql); 
                   ps.executeUpdate();
        } catch (SQLException ex) {
            ex.getMessage();
        }finally{
            close();
        } 
   }
}
