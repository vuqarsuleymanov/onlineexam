/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.mastersoft.rufan;

import java.io.Serializable;

/**
 *
 * @author Rufan
 */            
public class UsersPojo implements Serializable{
   
    private int id;
    private String firstname;
    private String lastname;  
    private String middlename;
    private String phone;
    private String email;      
    private String password;
    private String usercode;
    private String zone;      
    private String gender;
    private String birth_date;
    private String photo;
    private String univercity;
    private int active;
    private String topics;
    private String comments;
    private String content;

    public UsersPojo(int id, String firstname, String lastname, String middlename, String phone, String email, String password, String usercode, String zone, String gender, String birth_date, String photo, String univercity, int active, String topics, String comments, String content) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.usercode = usercode;
        this.zone = zone;
        this.gender = gender;
        this.birth_date = birth_date;
        this.photo = photo;
        this.univercity = univercity;
        this.active = active;
        this.topics = topics;
        this.comments = comments;
        this.content = content;
    }
    
    

    
    
    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
    public String getTopics() {
        return topics;
    }
    
    public void setTopics(String topics) {
        this.topics = topics;
    }

    public  void setActive(int active) {
        this.active = active;
    }

    public int getActive() {
        return active;
    }

    public String getUnivercity() {
        return univercity;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setUnivercity(String univercity) {
        this.univercity = univercity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }
    
    public UsersPojo() {
    }

 
   

    public String getComments() {
        return comments;
    }

    public UsersPojo(int active) {
        this.active = active;
    }
    public String  getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthDate(String birth_date) {
        this.birth_date = birth_date;
    }
    
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getZone() {
        return zone;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDate() {
        return birth_date;
    }
    
    public String getPhoto() {
        return photo;
    }
    
    
}
