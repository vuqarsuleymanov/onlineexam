package az.mastersoft.javanshir;

//@author javanshir
public class AnswerPojo {

    private int fk_question;
    private String answer;
    private int status;

    public AnswerPojo(int fk_question, String answer, int status) {
        this.fk_question = fk_question;
        this.answer = answer;
        this.status = status;
    }

    public AnswerPojo() {

    }

    public void setFk_question(int fk_question) {
        this.fk_question = fk_question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getFk_question() {
        return fk_question;
    }

    public String getAnswer() {
        return answer;
    }

    public int getStatus() {
        return status;
    }
}
