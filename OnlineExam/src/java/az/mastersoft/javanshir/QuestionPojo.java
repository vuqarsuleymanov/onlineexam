package az.mastersoft.javanshir;

//@author javanshir
public class QuestionPojo {

    private String question;
    private String difficulty_type;
    private String right_answer;
    private int status;
    private String subject;

    public QuestionPojo(String question, String difficulty_type, String right_answer, int status, String subject) {
        this.question = question;
        this.difficulty_type = difficulty_type;
        this.right_answer = right_answer;
        this.status = status;
        this.subject = subject;
    }

    public QuestionPojo() {

    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setDifficulty_type(String difficulty_type) {
        this.difficulty_type = difficulty_type;
    }

    public void setRight_answer(String right_answer) {
        this.right_answer = right_answer;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getQuestion() {
        return question;
    }

    public String getDifficulty_type() {
        return difficulty_type;
    }

    public String getRight_answer() {
        return right_answer;
    }

    public int getStatus() {
        return status;
    }

    public String getSubject() {
        return subject;
    }
}
