package az.mastersoft.javanshir;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//@author Javanshir
//suallarin strukturasi
//**cavablar ya radio button ya link yadaki img button
//**3 qisa sual yarad
//suallara cavablar duzalt
// table yarat (oz idsi, sual id, 5 dana column(cavab))
//suallari insert elamaycun sayfa ac (sonra)
public class DAO {

    private static Connection con = null;
    private static ResultSet rs = null;
    private static PreparedStatement pst = null;
    public static int totalRows = 0;

    public static String date_format(String input) {
        try {
            if (input == null) {
                return null;
            }

            DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = inputFormatter.parse(input);

            DateFormat outputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String output = outputFormatter.format(date);
            return output;
        } catch (ParseException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void connected() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/exam?characterEncoding=UTF-8", "vuqar", "admin");
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void close() {
        try {
            pst.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static List<QuestionPojo> question() {
        try {
            List<QuestionPojo> list = new ArrayList<>();
            String sql = "SELECT id, question, difficulty_type, right_answer, status, subject FROM questions";
            connected();
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                QuestionPojo t = new QuestionPojo();
                t.setQuestion(rs.getString(1));
                t.setDifficulty_type(rs.getString(2));
                t.setStatus(rs.getInt(3));
                t.setSubject(rs.getString(4));
                list.add(t);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }
    
    public static List<QuestionPojo> question_Find(String x) {
        try {
            List<QuestionPojo> list = new ArrayList<>();
            String sqlFind = "SELECT question, difficulty_type, status FROM questions WHERE subject = '" + x + "'";
            connected();
            pst = con.prepareStatement(sqlFind);
            rs = pst.executeQuery();
            while (rs.next()) {
                QuestionPojo t = new QuestionPojo();
                t.setQuestion(rs.getString(1));
                t.setDifficulty_type(rs.getString(2));
                t.setStatus(rs.getInt(3));
                list.add(t);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static List<AnswerPojo> answer() {
        try {
            List<AnswerPojo> list = new ArrayList<>();
            String sql = "SELECT id, fk_question, answer, status FROM answers";
            connected();
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                AnswerPojo t = new AnswerPojo();
                t.setFk_question(rs.getInt(1));
                t.setAnswer(rs.getString(2));
                t.setStatus(rs.getInt(3));
                list.add(t);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }
    
    public static int getCountOfQuestions() {
        try {
            connected();
            int count = 0;
            String sql = "select count(*) from questions";
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            return count;

        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } finally {
            close();
        }

    }
    
    public static int getCountOfAnswers() {
        try {
            connected();
            int count = 0;
            String sql = "select count(*) from answers";
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            return count;

        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } finally {
            close();
        }

    }

    public static List<TopResultPojo> topResult(int start) {
        try {
            List<TopResultPojo> list = new ArrayList<>();
            String sql = "SELECT exam.users.firstname, exam.users.lastname, exam.result.tarix, exam.result.netice "
                    + "FROM exam.users "
                    + "JOIN exam.result "
                    + "ON exam.users.id = exam.result.user_id "
                    + "ORDER BY netice DESC "
                    + "LIMIT " + start + ",  " + 10;
            connected();
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                TopResultPojo t = new TopResultPojo();
                t.setFirstname(rs.getString(1));
                t.setLastname(rs.getString(2));
                t.setTarix(date_format(rs.getString(3)));
                t.setNetice(rs.getDouble(4));
                list.add(t);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }
    
    public static List<TopResultPojo> topResult_Find(int start, int user_id) {
        try {
            List<TopResultPojo> list = new ArrayList<>();
            String sql = "SELECT exam.users.firstname, exam.users.lastname, exam.result.tarix, exam.result.netice "
                    + "FROM exam.users "
                    + "JOIN exam.result "
                    + "ON exam.users.id = exam.result.user_id "
                    + "WHERE exam.users.id = " + user_id + " "
                    + "ORDER BY netice DESC "
                    + "LIMIT " + start + ",  " + 10;
            connected();
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                TopResultPojo t = new TopResultPojo();
                t.setFirstname(rs.getString(1));
                t.setLastname(rs.getString(2));
                t.setTarix(date_format(rs.getString(3)));
                t.setNetice(rs.getDouble(4));
                list.add(t);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static int getCountOfTopResult() {
        try {
            connected();
            int count = 0;
            String sql = "select count(*) from result";
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            if (count % 10 == 0) {
                return count / 10;
            } else {
                return count / 10 + 1;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } finally {
            close();
        }

    }

    public static void main(String[] args) {

    }
}
