package az.mastersoft.javanshir;

import java.io.Serializable;

//@author Javanshir
public class TopResultPojo implements Serializable {

    private String firstname;
    private String lastname;
    private String tarix;
    private double netice;

    public TopResultPojo() {
    }

    public TopResultPojo(String firstname, String lastname, String tarix, double netice) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.tarix = tarix;
        this.netice = netice;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTarix() {
        return tarix;
    }

    public void setTarix(String tarix) {
        this.tarix = tarix;
    }

    public double getNetice() {
        return netice;
    }

    public void setNetice(double netice) {
        this.netice = netice;
    }

}
