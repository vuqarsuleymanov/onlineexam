<%@page import="az.mastersoft.javanshir.QuestionPojo"%>
<%@page import="az.mastersoft.javanshir.DAO"%>
<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Ana sehife</title>

        <!-- Core CSS - Include with every page -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Blank -->
        <link href="css/jquery-ui.css" rel="stylesheet">
        <link href="css/jquery-ui.min.css" rel="stylesheet">
        <link href="bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">

        <!-- SB Admin CSS - Include with every page -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Bootstrap vertical tabs -->
        <link rel="stylesheet" href="css/bootstrap-vertical-tabs-1.1.0/bootstrap.vertical-tabs.css">

        <style>
            .no-close .ui-dialog-titlebar-close {display: none }
        </style>

    </head>
    <%@include file="left_home.jsp"%>

    <body>

        <div id="box">
            <select id="language" class="selectpicker">
                <option value="en">Ingilis Dili</option>
                <option value="ru">Rus Dili</option>
            </select>
        </div>

        <div id="wrapper">

            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">

                <%@include file="top_home.jsp"%>

            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <script>
                        function changeOption() {
                            if ($("#language").val() === "en") {
                                $("#en").show();
                            } else if ($("#language").val() === "ru") {
                                $("#ru").show();
                            }
                        }
                    </script>
                    <div class="col-lg-12">
                        <div class="input-group">
                            <h3>
                                Fənn: 
                                <div id="en" style="display: none">
                                    <select class="ing selectpicker">
                                        <option value="informatika" selected="selected">Informatika</option>
                                        <option value="mentiq">Mentiq</option>
                                        <option value="ing-dili">Ingilis Dili</option>
                                    </select>
                                </div>
                                <div id="ru" style="display: none">
                                    <select class="rus selectpicker">
                                        <option value="informatika" selected="selected">Informatika</option>
                                        <option value="mentiq">Mentiq</option>
                                        <option value="rus-dili">Rus Dili</option>
                                    </select>
                                </div>
                                <hr width="967px">
                            </h3>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function() {
                            $(".firsttab").show();
                            $(".secondtab").hide();
                            $(".thirdtab").hide();
                            $(".fourthtab").hide();

                            $("#en").change(function() {
                                if ($(".ing").val() === "informatika") {
                                    $(".firsttab").show();
                                    $(".secondtab").hide();
                                    $(".thirdtab").hide();
                                    $(".fourthtab").hide();
                                } else if ($(".ing").val() === "mentiq") {
                                    $(".firsttab").hide();
                                    $(".secondtab").show();
                                    $(".thirdtab").hide();
                                    $(".fourthtab").hide();
                                } else if ($(".ing").val() === "ing-dili") {
                                    $(".firsttab").hide();
                                    $(".secondtab").hide();
                                    $(".thirdtab").show();
                                    $(".fourthtab").hide();
                                }
                            });
                            $("#ru").change(function() {
                                if ($(".rus").val() === "informatika") {
                                    $(".firsttab").show();
                                    $(".secondtab").hide();
                                    $(".thirdtab").hide();
                                    $(".fourthtab").hide();
                                } else if ($(".rus").val() === "mentiq") {
                                    $(".firsttab").hide();
                                    $(".secondtab").show();
                                    $(".thirdtab").hide();
                                    $(".fourthtab").hide();
                                } else if ($(".rus").val() === "rus-dili") {
                                    $(".firsttab").hide();
                                    $(".secondtab").hide();
                                    $(".thirdtab").hide();
                                    $(".fourthtab").show();
                                }
                            });
                        });
                    </script>
                    <div class="tablar">
                        <div class="firsttab">
                            <div class="col-xs-9">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <%  int j = 1;
                                        for (QuestionPojo t : DAO.question_Find("informatika")) {
                                            if (j == 1) {%>
                                    <div class="tab-pane active" id="f<%=j%>"><%=j%>) <%=t.getQuestion()%></div>
                                    <%} else {%>
                                    <div class="tab-pane" id="f<%=j%>"><%=j%>) <%=t.getQuestion()%></div>
                                    <%}
                                            j++;
                                        }%>
                                </div>
                            </div>

                            <div class="col-xs-3"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-right">
                                    <%int i = 1;
                                        for (QuestionPojo t : DAO.question_Find("informatika")) {
                                            if (i == 1) {%>
                                    <li class="active"><a href="#f<%=i%>" data-toggle="tab"><%=i%>) <%=t.getQuestion()%></a></li>
                                        <%} else {%>
                                    <li><a href="#f<%=i%>" data-toggle="tab"><%=i%>) <%=t.getQuestion()%></a></li>
                                        <%}
                                                i++;
                                            }%>
                                </ul>
                            </div>
                        </div>


                        <div class="secondtab">
                            <div class="col-xs-9">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <%  for (int a = 1; a < 10; a++) {
                                            if (a == 1) {%>
                                    <div class="tab-pane active" id="s<%=a%>">Home2 Tab.</div>
                                    <%} else {%>
                                    <div class="tab-pane" id="s<%=a%>">Profile2 <%=a%>Tab.</div>
                                    <%}
                                        }%>
                                </div>
                            </div>

                            <div class="col-xs-3"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-right">
                                    <%for (int b = 1; b < 10; b++) {
                                            if (b == 1) {%>
                                    <li class="active"><a href="#s<%=b%>" data-toggle="tab">Home2 dknfvsgbdfgbdfbgdfbgdf</a></li>
                                        <%} else {%>
                                    <li><a href="#s<%=b%>" data-toggle="tab">Profile2 <%=b%>gdfsgfdsgrfvdctbgdf</a></li>
                                        <%}
                                            }%>
                                </ul>
                            </div>
                        </div>


                        <div class="thirdtab">
                            <div class="col-xs-9">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <%  for (int c = 1; c < 6; c++) {
                                            if (c == 1) {%>
                                    <div class="tab-pane active" id="t<%=c%>">Home3 Tab.</div>
                                    <%} else {%>
                                    <div class="tab-pane" id="t<%=c%>">Profile3 <%=c%>Tab.</div>
                                    <%}
                                        }%>
                                </div>
                            </div>

                            <div class="col-xs-3"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-right">
                                    <%for (int d = 1; d < 6; d++) {
                                            if (d == 1) {%>
                                    <li class="active"><a href="#t<%=d%>" data-toggle="tab">Home3 dknfvsgbdfgbdfbgdfbgdf</a></li>
                                        <%} else {%>
                                    <li><a href="#t<%=d%>" data-toggle="tab">Profile3 <%=d%>gdfsgfdsgrfvdctbgdf</a></li>
                                        <%}
                                            }%>
                                </ul>
                            </div>
                        </div>

                        <div class="fourthtab">
                            <div class="col-xs-9">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <%  for (int e = 1; e < 6; e++) {
                                            if (e == 1) {%>
                                    <div class="tab-pane active" id="r<%=e%>">Home4 Tab.</div>
                                    <%} else {%>
                                    <div class="tab-pane" id="r<%=e%>">Profile4 <%=e%>Tab.</div>
                                    <%}
                                        }%>
                                </div>
                            </div>

                            <div class="col-xs-3"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-right">
                                    <%for (int f = 1; f < 6; f++) {
                                            if (f == 1) {%>
                                    <li class="active"><a href="#r<%=f%>" data-toggle="tab">Home4 dknfvsgbdfgbdfbgdfbgdf</a></li>
                                        <%} else {%>
                                    <li><a href="#r<%=f%>" data-toggle="tab">Profile4 <%=f%>gdfsgfdsgrfvdctbgdf</a></li>
                                        <%}
                                            }%>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr width="967px">
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Core Scripts - Include with every page -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Page-Level Plugin Scripts - Blank -->

        <!-- SB Admin Scripts - Include with every page -->
        <script src="js/sb-admin.js"></script>

        <!-- Page-Level Demo Scripts - Blank - Use for reference -->
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    </body>

</html>
