<!DOCTYPE html>
<html>
 <%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Ana sehife</title>

    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<style type="text/css">
    .messages{
    	margin: 20px;
    }
</style>
</head>
        <%@include file="left_home.jsp"%>
<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            
         <%@include file="top_home.jsp"%>
         
        </nav>
          
        
         
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Mesaj</h1>
                </div>
              
            </div>
          <div class="messages">         
     <div class="media" style="background-color: buttonface">
        <a href="#" class="pull-left">
            <img src="img/student_avatar.png" class="media-object img-thumbnail" alt="Sample Image" style="width: 90px; height: 90px;">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Tələbə <small><i> 8 Avqust, 2014</i></small></h4>
            <p>Salam.
                Mənim sualım polinomlarla bağlıdır.
                Zəhmət olmasa yazdğım sualı cavablandırardınız.
                Öncədən Təşəkkürlər! </p>
        </div>
    </div>
    <hr />
    <div class="media" style="background-color: buttonface" >
        <a href="#" class="pull-right">
            <img src="img/teacher_avatar.png" class="media-object img-thumbnail" alt="Sample Image" style="width: 90px; height: 90px;">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Müəllim <small><i> 8 Avqust, 2014</i></small></h4>
            <p>Salam.
               Sizin sualınızla bağlı material və nümunələr artiq saytımızda yerləşdirilib.
               Baxıb faydalana bilərsiniz.
               Uğurlar!</p>
        </div>
    </div>
    <br/>
    <div class="span6"> 
        <textarea class="field span12 form-control" style="background-color: buttonface"  rows="3"  placeholder="Mesaj yaz..."></textarea>
        <br/>
        <button class="btn btn-default pull-right">Göndər</button>
    </div>
</div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->

</body>

</html>
