<%@page import="az.mastersoft.natiq.DBOperations"%>
<%@page import="az.mastersoft.rufan.UsersPojo"%>
<%@page language="java" session="true" %>
<!DOCTYPE html>
<html>
 <%@page contentType="text/html" pageEncoding="UTF-8"%>
 <% int id = Integer.parseInt(session.getAttribute("user_id").toString());
    UsersPojo up = DBOperations.getInfo(id);
    
    String gen;
    if(up.getGender().equals("K")){
      gen = "Kişi";
    }else{
      gen = "Qadın";
    }
 %>
 
 <%  
   String error = (String) request.getAttribute("imgError");  
   if ( error != null ) { %>  
   <script> alert("Düzgün şəkil formatı əlavə edin.");</script>  
   <% } 
 %>
 
 
<head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><%=up.getFirstname() +" "+ up.getLastname() %></title>

    <!-- FileInputStyle -->
    
    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
       
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>   
    
    <script>
         $(document).ready(function(){
            $("button").click(function(){
                $('#edit').hide();
                $('#save').show();
                
                $('#labelFName').hide();
                $('#txtFName').show();
                
                $('#labelLName').hide();
                $('#txtLName').show();
                
                $('#comboGender').show();
                $('#labelGender').hide();
                
                $('#labelAddress').hide();
                $('#txtZone').show();
                
                $('#labelPhone').hide();
                $('#txtPhone').show();
                
            });
         });
         
         $(document).ready(function(){
         $("#avatar").hover(function(){
             $("#selectedAvatar").fadeIn(500);
    }, function(){
        // change to any color that was previously used.
        $("#selectedAvatar").fadeOut(500);
    });
    
    $("#selectedAvatar").click(function (){
        $("#file").trigger('click');
    });
    
    $("#file").change(function (){
        
    var maxSize = 4 * 1024 * 1024;        
    var fileSize = this.files[0].size; 
      
        if(fileSize < maxSize){
        $("#uploadForm").submit();
        }else{
            alert("Şəklin həcmi olduqca böyükdür.");
        }
    });
    
         });   
         
         /* Initialize your widget via javascript as follows */
                 
         
         
    </script>  
    
    <script language="javascript">
             
     function myfunction(){         
         
         var person={};
         
         person["name"] = document.getElementById('txtFName').value;
         person["sname"] = document.getElementById('txtLName').value;
         person["gender"] = document.getElementById('txtGender').value;
         person["zone"] = document.getElementById('txtZone').value;
         person["phone"] = document.getElementById('txtPhone').value;
         
         var json = JSON.stringify(person);
         
     }
    </script>    
    
</head>
        <%@include file="left_home.jsp"%>
<body>
    <div id="wrapper"> 

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            
         <%@include file="top_home.jsp"%>
         
        </nav>
          
        
         
         <div id="page-wrapper">
             <div class="row">
                 <div class="col-lg-12">
                     <h1 class="page-header">Şəxsi məlumat</h1>
                 </div>
                 
                 <div class="row pull-left" style="margin-left: 1%">
                       <!--  <div class="col-md-5  toppad  pull-left col-md-offset-3 ">
                             <A href="edit.html" >Profil düzənlə</A>

                             <A href="edit.html" >Çıxış</A>
                             <br>
                             <p class=" text-info">May 05,2014,03:00 pm </p>
                         </div> -->
                                                
                       <form id="uploadForm" action="upload" method="POST" enctype="multipart/form-data">
                           <input style="display: none" type="file" name="file" id="file" accept="image/x-png, image/gif, image/jpeg" /><br>                               
                       </form>
                                             
                       <form method="post" action="RefreshDB">
                             <div class="panel panel-info pull-left">
                                 <div class="panel-heading">
                                     <h3 class="panel-title"><%=up.getLastname() + " " + up.getFirstname()%> </h3>
                                 </div>
                                 <div class="panel-body" style="width: 500px; height: 300px">
                                     <div class="row">
                                         
                                         <div id="avatar"  class="pull-left" width="100" height="100" align="center" > 
                                             
                                             <div id="selectedAvatar" style="display: none; background-color: black; cursor: pointer; width: 90px; height: 20px; opacity: 0.8; position: absolute;margin-top: 75px ; margin-left: 5px; border-radius: 10px; text-align: left; vertical-align: middle">
                                                 <i style=" position: absolute; z-index: 1000;  color: #fff; margin-left: 37px;margin-top: 2px;opacity: 1.0; " class="glyphicon glyphicon-camera " ></i>
                                             </div>
                                             
                                             <img alt="User Pic" src="<%=up.getPhoto()%>" class="thumbnail" style="border-radius: 15%" width="100" height="100">
                                              
                                         </div>
                                         <div class=" pull-left">
                                             <table class="table table-user-information" style="margin-left: 1%; width: 380px;">
                                                 <tbody>
                                                     <tr style="height: 51px;">
                                                         <td>Adı:</td>
                                                         <td>
                                                             <input type="text" value="<%= up.getFirstname()%>" name="name" class="form-control" id="txtFName" style="display:none;"/>
                                                             <label id="labelFName"><%= up.getFirstname()%></label>
                                                         </td>
                                                     </tr>
                                                     <tr style="height: 51px;">
                                                         <td>Soyadı:</td>
                                                         <td>
                                                             <input type="text" class="form-control" name="surname" value="<%= up.getLastname()%>" id="txtLName" style="display:none;"/>
                                                             <label id="labelLName"><%= up.getLastname()%></label>
                                                         </td>
                                                     </tr>

                                                     <tr style="height: 51px;">
                                                         <td>Təvəllüd:</td>
                                                         <td> <label id="labelBirthDate"><%= up.getBirthDate()%></label>
                                                             <input name="date" type="hidden" value="<%=up.getBirthDate() %>"
                                                         </td>     
                                                     </tr>

                                                     <tr style="height: 51px;">
                                                         <td>Cins:</td>
                                                         <td>
                                                             <input name="gender" type="text" class="form-control" value="<%= up.getGender()%>" id="comboGender" style="display:none;"/>                                                             <label id="labelGender"><%=gen%></label>
                                                         </td>
                                                     </tr>
                                                     <tr style="height: 51px;">
                                                         <td>Ünvan:</td>
                                                         <td>
                                                             <input name="zone" type="text" class="form-control" value="<%= up.getZone()%>" id="txtZone" style="display:none;"/>
                                                             <label id="labelAddress"><%= up.getZone()%></label>
                                                         </td>
                                                     </tr>
                                                     
                                                     <tr style="height: 51px;">
                                                         <td>Email:</td>
                                                         <td> <label id="labelEmail"> <a href="mailto:<%=up.getEmail() %>"><%=up.getEmail()%></a> </label>
                                                             <input type="hidden" name="email" value="<%=up.getEmail() %>" />
                                                         </td> 
                                                     </tr>
                                                     
                                                     <tr style="height: 51px;">
                                                         <td>Telefon nömrəsi: </td>
                                                         <td>
                                                             <input name="phone" type="text" value="<%= up.getPhone()%>" class="form-control" id="txtPhone" style="display:none;"/>
                                                             <label id="labelPhone"><%= up.getPhone()%></label>
                                                         </td>
                                                     </tr>                                                
                                                 </tbody>
                                             </table>
                                         </div>
                                     </div>
                                 </div>
                                                         <br><br><br><br>
                                 <div class="panel-footer">                                
                                     <a data-original-title="Broadcast Message" title="Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                              
                                     <span class="pull-right"> 
                                         <button id="save" name="save" id="btnSave" title="Save" onclick="myfunction()" data-original-title="Save" style="display: none;" data-toggle="tooltip" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-floppy-saved"></i></button>
                                         <button onclick="return false;" id="edit" title="Edit" data-original-title="Edit this user" data-toggle="tooltip" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></button>                                         
                                     </span>
                                 </div>

                             </div>
                       </form>
                     </div>
                
             </div>
             <!-- /.row -->
         </div>
         <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>
   
    <!-- Page-Level Demo Scripts - Blank - Use for reference -->

</body>

</html>
