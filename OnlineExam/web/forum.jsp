<%-- 
    Document   : forum
    Created on : Aug 31, 2014, 10:09:59 PM
    Author     : Rufan
--%>

<%@page import="az.mastersoft.rufan.ForumPojo"%>
<%@page import="az.mastersoft.rufan.DBO"%>
<%@page import="az.mastersoft.rufan.UsersPojo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="http://forum.azyrusthemes.com/css/custom.css" rel="stylesheet">
        <link href="css/css" rel="stylesheet" type="text/css">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen">
        <script src="js/bootstrap.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>  
        
         <%  
            int sheet = 1;
            int last = 1;
            int last2=1;
            if (request.getParameter("page") != null ) {
                sheet = Integer.parseInt(request.getParameter("page"));
            }

            int start = (sheet - 1) * 10;
        %>
        
    </head>
    <body>
       
          <div class="container-fluid">
 <%@include file="top_home.jsp" %>
            <!-- Slider -->
            <div class="tp-banner-container" style="overflow: visible;">
                <div class="tp-banner revslider-initialised tp-simpleresponsive" id="revslider-537" style="max-height: 278px; height: 278px;">
                    <ul style="display: block; overflow: hidden; width: 100%; height: 100%; max-height: none;">	
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" style="width: 100%; height: 100%; overflow: hidden; visibility: visible; left: 0px; top: 0px; z-index: 20; opacity: 1;">
                            <!-- MAIN IMAGE -->
                            <div class="slotholder" style="width:100%;height:100%;"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazydone="undefined" data-src="images/slide.jpg" style="width: 100%; height: 100%; opacity: 1; position: relative; background-image: url(img/slide.jpg); background-color: rgba(0, 0, 0, 0); background-size: cover; background-position: 0% 0%; background-repeat: no-repeat;"></div></div>
                            <!-- LAYERS -->
                        </li>
                    </ul>
                <div class="tp-loader" style="display: none;"></div><div class="tp-bannertimer" style="visibility: hidden; width: 0%;"></div></div>
            </div>
            <!-- //Slider -->
             <div class="headernav">
                <div class="container">
                    <div class="row">
                        
                        <div class="col-lg-3 col-xs-9 col-sm-5 col-md-3 selecttopic" >
                           
                        </div>
                        <div class="col-lg-4 search hidden-xs hidden-sm col-md-3">
                            <div class="wrap">
                                <form action="#" method="post" class="form">
                                    <div class="pull-left txt"><input type="text" class="form-control" placeholder="Mövzu axtar"></div>
                                    <div class="pull-right"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-sm-5 col-md-4 avt">
                            <div class="stnt pull-left">                            
                                <form action="03_new_topic.html" method="post" class="form">
                                    <button class="btn btn-primary">Sual ver</button>
                                </form>
                            </div>
                           

                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xs-12 col-md-8">
                             
                             <% if (sheet != 1) {%>
                            <div class="pull-left"><a href="forum.jsp?page=<%=sheet - 1%>" class="prevnext"><i class="fa fa-angle-left"></i></a></div> 
                                <%} %> 
                            <div class="pull-left">
                                <ul class="paginationforum">
                            
                                    <% for (int i = 1; i <= DBO.getCountOfTopics(); i++) {
                                    last += 1; %>
                                        <% if (sheet == i ) {%>
                                    <li><a class="active" href="forum.jsp?page=<%=i%>"><%=i%></a></li>
                                        <% } else {%>
                                    <li ><a class=""  href="forum.jsp?page=<%=i %>"><%=i%></a></li>
                                        <% }
                                            }%>                                                                                         
                                </ul>
                            </div>
                              <% if (last-1 != sheet) {%>
                            <div class="pull-left"><a href="forum.jsp?page=<%=sheet + 1%>" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                            <% } %>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>


                <div class="fullscreen-container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8">
                            <!-- POST -->
                            <% 
                                for(ForumPojo fp:DBO.Topics(start)){
                                UsersPojo up = DBO.getUserById(fp.getFk_user());
                                 int count=DBO.getCountOfComments2(fp.getId());
                                            
                                            
                            %>
                            <div class="post" style="margin-left: 20px">
                                <div class="wrap-ut pull-left">
                                    <div class="userinfo pull-left">
                                        <div class="avatar">
                                            <img style="width: 37px; height: 37px;" src="<%=up.getPhoto() %>" alt="">
                                            <div class="status green">&nbsp;</div>
                                        </div>

                                            <div class="icons">
                                            <img src="Forum/icon1.jpg" alt=""><img src="Forum/icon4.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="posttext pull-left">
                                         <%= up.getFirstname()+ " "+ up.getLastname() %>
                                         <h2><a id="<%= fp.getId() %>" href="forumDetails.jsp?id=<%= fp.getId() %>" style="font-family: New Times Roman"  ><%=  fp.getTitle()%></a></h2>
                                        
                                         
                                     
                                        <p><%=  fp.getContent()%></p> 
                                   
                                        
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="postinfo pull-left">
                                    <div class="comments">
                                        <div class="commentbg">
                                            
                                            <%= count %>
                                            <div class="mark"></div>
                                        </div>

                                    </div> <% int view_count=DBO.view(fp.getId()); %>
                                    <div  id="view" class="views"><i class="fa fa-eye"></i>&nbsp;<%=view_count %></div>
                                  
                                    <div class="time"><i class="fa fa-clock-o"></i> <%=fp.getInsertDate()  %></div>                                    
                                </div>
                                <div class="clearfix"></div> 
                            </div><!-- POST -->
                            <% 
                                }
                            %>
                                
                            

                        </div>
                        <div class="col-lg-4 col-md-4" style="margin-right: 0px;">

                            <!-- -->
                            <div class="sidebarblock">
                                <h3>Categories</h3>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <ul class="cats">
                                        <li><a href="index.htm">Trading for Money <span class="badge pull-right">20</span></a></li>
                                        <li><a href="index.htm">Vault Keys Giveway <span class="badge pull-right">10</span></a></li>
                                        <li><a href="index.htm">Misc Guns Locations <span class="badge pull-right">50</span></a></li>
                                        <li><a href="index.htm">Looking for Players <span class="badge pull-right">36</span></a></li>
                                        <li><a href="index.htm">Stupid Bugs &amp; Solves <span class="badge pull-right">41</span></a></li>
                                        <li><a href="index.htm">Video &amp; Audio Drivers <span class="badge pull-right">11</span></a></li>
                                        <li><a href="index.htm">2K Official Forums <span class="badge pull-right">5</span></a></li>
                                    </ul>
                                </div>
                            </div>

                            <!-- -->
                            <div class="sidebarblock">
                                <h3>Poll of the Week</h3>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <p>Which game you are playing this week?</p>
                                    <form action="index.htm" method="post" class="form">
                                        <table class="poll">
                                            <tbody><tr>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar color1" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                                            Call of Duty Ghosts
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="chbox">
                                                    <input id="opt1" type="radio" name="opt" value="1">  
                                                    <label for="opt1"></label>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar color2" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 63%">
                                                            Titanfall
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="chbox">
                                                    <input id="opt2" type="radio" name="opt" value="2" checked="">  
                                                    <label for="opt2"></label>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar color3" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
                                                            Battlefield 4
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="chbox">
                                                    <input id="opt3" type="radio" name="opt" value="3">  
                                                    <label for="opt3"></label>  
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </form>
                                    <p class="smal">Voting ends on 19th of October</p>
                                </div>
                            </div>

                            <!-- -->
                            <div class="sidebarblock">
                                <h3>My Active Threads</h3>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="index.htm">This Dock Turns Your iPhone Into a Bedside Lamp</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="index.htm">Who Wins in the Battle for Power on the Internet?</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="index.htm">Sony QX10: A Funky, Overpriced Lens Camera for Your Smartphone</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="index.htm">FedEx Simplifies Shipping for Small Businesses</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="index.htm">Loud and Brave: Saudi Women Set to Protest Driving Ban</a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>



               <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xs-12 col-md-8">
                             
                             <% if (sheet != 1) {%>
                            <div class="pull-left"><a href="forum.jsp?page=<%=sheet - 1%>" class="prevnext"><i class="fa fa-angle-left"></i></a></div> 
                                <%} %> 
                            <div class="pull-left">
                                <ul class="paginationforum">
                            
                                    <% for (int i = 1; i <= DBO.getCountOfTopics(); i++) {
                                    last2 += 1; %>
                                        <% if (sheet == i ) {%>
                                    <li><a class="active" href="forum.jsp?page=<%=i%>"><%=i%></a></li>
                                        <% } else {%>
                                    <li ><a class=""  href="forum.jsp?page=<%=i %>"><%=i%></a></li>
                                        <% }
                                            }%>                                                                                         
                                </ul>
                            </div>
                              <% if (last2-1 != sheet) {%>
                            <div class="pull-left"><a href="forum.jsp?page=<%=sheet + 1%>" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                            <% } %>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>


            </section>

           
        </div>

        <!-- get jQuery from the google apis -->
        <script src="js/jquery-1.10.2.js"></script>
    
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>
 

        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>

        
    </body>
</html>
