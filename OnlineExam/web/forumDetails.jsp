<%-- 
    Document   : forum
    Created on : Aug 31, 2014, 10:09:59 PM
    Author     : Rufan
--%>

<%@page import="az.mastersoft.rufan.ForumPojo"%>
<%@page import="az.mastersoft.rufan.CommentPojo"%>
<%@page import="az.mastersoft.rufan.DBO"%>
<%@page import="az.mastersoft.rufan.UsersPojo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% if (request.getParameter("id") != null) {
        int id = Integer.parseInt(request.getParameter("id"));
%>
<html lang="en"><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%ForumPojo fp = DBO.getForumById(id);%>
        <title><%=fp.getTitle()%></title>

        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="http://forum.azyrusthemes.com/css/custom.css" rel="stylesheet">
        <link href="css/css" rel="stylesheet" type="text/css">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen">
        <script src="js/bootstrap.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>  
         <%
            int sheet = 1;
            int last = 1;
            int last2=1;
            if (request.getParameter("page") != null ) {
                sheet = Integer.parseInt(request.getParameter("page"));
            }

            int start = (sheet - 1) * 10;
        %>
    
  
       
           <% int view_count=DBO.view(id);
              ++view_count;
              DBO.insert_view_count(id, view_count);
            %>
         
            <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/az_AZ/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
            
    </head>
    <body class="topic">

        <div class="container-fluid"> 
            <%@include file="top_home.jsp" %>
            <!-- Slider -->
           <div class="tp-banner-container" style="overflow: visible;">
                <div class="tp-banner revslider-initialised tp-simpleresponsive" id="revslider-537" style="max-height: 278px; height: 278px;">
                    <ul style="display: block; overflow: hidden; width: 100%; height: 100%; max-height: none;">	
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" style="width: 100%; height: 100%; overflow: hidden; visibility: visible; left: 0px; top: 0px; z-index: 20; opacity: 1;">
                            <!-- MAIN IMAGE -->
                            <div class="slotholder" style="width:100%;height:100%;"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazydone="undefined" data-src="images/slide.jpg" style="width: 100%; height: 100%; opacity: 1; position: relative; background-image: url(img/slide.jpg); background-color: rgba(0, 0, 0, 0); background-size: cover; background-position: 0% 0%; background-repeat: no-repeat;"></div></div>
                            <!-- LAYERS -->
                        </li>
                    </ul>
                <div class="tp-loader" style="display: none;"></div><div class="tp-bannertimer" style="visibility: hidden; width: 0%;"></div></div>
            </div>
            <!-- //Slider -->


            <div class="headernav">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-xs-9 col-sm-5 col-md-3 selecttopic" >

                        </div>
                        <div class="col-lg-4 search hidden-xs hidden-sm col-md-3">
                            <div class="wrap">
                                <form action="#" method="post" class="form">
                                    <div class="pull-left txt"><input type="text" class="form-control" placeholder="Mövzu axtar"></div>
                                    <div class="pull-right"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-sm-5 col-md-4 avt">
                            <div class="stnt pull-left">                            
                                <form action="03_new_topic.html" method="post" class="form">
                                    <button class="btn btn-primary">Sual ver</button>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            

            <section class="content">
                <div class="container">
                    <div class="row">
                       <div class="col-lg-8 col-xs-12 col-md-8">
                             
                             <% if (sheet != 1) {%>
                            <div class="pull-left"><a href="forumDetails.jsp?page=<%=sheet - 1%>" class="prevnext"><i class="fa fa-angle-left"></i></a></div> 
                                <%} %> 
                            <div class="pull-left">
                                <ul class="paginationforum">
                            
                                    <% for (int i = 1; i <= DBO.getCountOfComments(); i++) {
                                    last += 1; %>
                                        <% if (sheet == i ) {%>
                                        <li><a class="active" href="forumDetails.jsp?page=<%=i%>&id=<%=fp.getId() %>" ><%=i%></a></li>
                                        <% } else {%>
                                    <li ><a class=""  href="forumDetails.jsp?page=<%=i %>&id=<%=fp.getId() %>"><%=i%></a></li>
                                        <% }
                                            }%>                                                                                         
                                </ul>
                            </div>
                              <% if (last-1 != sheet) {%>
                            <div class="pull-left"><a href="forumDetails.jsp?page=<%=sheet + 1%>&id=<%=fp.getId() %>" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                            <% } %>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 breadcrumbf">
                            <a href="#">Borderlands 2</a> <span class="diviver">&gt;</span> <a href="#">General Discussion</a> <span class="diviver">&gt;</span> <a href="#">Topic Details</a>
                        </div>
                    </div>
                </div>


                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8">

                            <!-- Sual -->
                            <%
                                UsersPojo up = DBO.getUserById(fp.getFk_user());
                                
                                
                            %>
                            <div class="post beforepagination">
                                <div class="topwrap">
                                    <div class="userinfo pull-left">

                                        <div class="avatar">
                                            <img  style="width: 37px; height: 37px;"  src="<%=up.getPhoto()%>">
                                            <div class="status green">&nbsp;</div>
                                        </div>

                                        <div class="icons">
                                            <img src="Forum/icon1.jpg" alt=""><img src="Forum/icon4.jpg" alt=""><img src="Forum/icon5.jpg" alt=""><img src="Forum/icon6.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="posttext pull-left">
                                        <%=up.getFirstname() + " " + up.getLastname()%>     <h2 style="font-family: New Times Roman"><%=fp.getTitle()%></h2>
                                        <p style="font-family: New Times Roman"><%=fp.getContent()%></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                              
                                <div class="postinfobot">

                                    <div class="likeblock pull-left">
                                        <a id="like" href="LikeUnlike?like&topic=<%=id%>" class="up"><i class="fa fa-thumbs-o-up"></i><%=DBO.getLikeCountByFkTopic(id,1)%></a> <!-- LIKE -->
                                        <a  id="unlike" href="LikeUnlike?unlike&topic=<%=id%>" class="down"><i class="fa fa-thumbs-o-down"></i><%=DBO.getLikeCountByFkTopic(id,0)%></a> <!-- Unlike -->
                                    </div>

                                    <div class="prev pull-left">                                        
                                       
                                    </div>

                                    <div class="posted pull-left"><i class="fa fa-clock-o"></i> Tarix : <%=fp.getInsertDate()%></div>

                                
                                                <div class="next pull-right">   
                                                     <script>
                                    function PopupOrtala(pageURL, title,w,h) {
                                    var left = (screen.width/2)-(w/2);
                                    var top = (screen.height/2)-(h/2);
                                    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                                    } 
                                    </script>
                                    <div class="fb_btn"> <a href="javascript:PopupOrtala('http://www.facebook.com/sharer.php?u=http://localhost:8085/OnlineExam/forumDetails.jsp?id=<%=id %>')"><i class="fa fa-facebook-square"> </i></a>
                                    </div>


                                   

                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div><!-- Sual -->

                            <div class="paginationf">
                                <br/>
                            </div>

                            <!-- Cavab -->
                            <% for (CommentPojo cp : DBO.comments(fp.getId(),start)) {
                                    UsersPojo up2 = DBO.getUserById(cp.getFk_user());
                                    
                                   
                            %>
                            <div class="post">
                                <div class="topwrap">
                                    <div class="userinfo pull-left">
                                        <div  style="width: 37px; height: 37px;" class="avatar">
                                            <img  style="width: 37px; height: 37px;" src="<%=up2.getPhoto()%>"> 
                                            <div class="status red">&nbsp;</div>
                                        </div>

                                        <div class="icons">
                                            <img src="Forum/icon3.jpg" alt=""><img src="Forum/icon4.jpg" alt=""><img src="Forum/icon5.jpg" alt=""><img src="Forum/icon6.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="posttext pull-left">
                                        <%=up2.getFirstname() + " " + up2.getLastname()%>
                                        <p style="font-family: New Times Roman"><%=cp.getComment()%></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                              
                                <div class="postinfobot">

                                    <div class="likeblock pull-left">
                                        <a id="clike" href="CommentLikeUnlike?like&topic=<%=id %>&comment=<%=cp.getId() %>" class="up"><i class="fa fa-thumbs-o-up"></i><%=DBO.getLikeCountByFkComment(cp.getId(),1)%></a> <!-- LIKE -->
                                        <a  id="cunlike" href="CommentLikeUnlike?unlike&topic=<%=id %>&comment=<%=cp.getId()%>" class="down"><i class="fa fa-thumbs-o-down"></i><%=DBO.getLikeCountByFkComment(cp.getId(),0)%></a> <!-- Unlike --> 
                                    </div>
                                    
                                   

                                    <div class="posted pull-left"><i class="fa fa-clock-o"></i> Tarix : <%=cp.getInsert_date()%></div>

                                   

                                    <div class="clearfix"></div>
                                </div>
                            </div><!-- Cavab -->
                            <% } %>


                        </div>
                        <div class="col-lg-4 col-md-4">

                            <!-- -->
                            <div class="sidebarblock">
                                <h3>Categories</h3>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <ul class="cats">
                                        <li><a href="#">Trading for Money <span class="badge pull-right">20</span></a></li>
                                        <li><a href="#">Vault Keys Giveway <span class="badge pull-right">10</span></a></li>
                                        <li><a href="#">Misc Guns Locations <span class="badge pull-right">50</span></a></li>
                                        <li><a href="#">Looking for Players <span class="badge pull-right">36</span></a></li>
                                        <li><a href="#">Stupid Bugs &amp; Solves <span class="badge pull-right">41</span></a></li>
                                        <li><a href="#">Video &amp; Audio Drivers <span class="badge pull-right">11</span></a></li>
                                        <li><a href="#">2K Official Forums <span class="badge pull-right">5</span></a></li>
                                    </ul>
                                </div>
                            </div>

                            <!-- -->
                            <div class="sidebarblock">
                                <h3>Poll of the Week</h3>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <p>Which game you are playing this week?</p>
                                    <form action="#" method="post" class="form">
                                        <table class="poll">
                                            <tbody><tr>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar color1" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                                                Call of Duty Ghosts
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="chbox">
                                                        <input id="opt1" type="radio" name="opt" value="1">  
                                                        <label for="opt1"></label>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar color2" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 63%">
                                                                Titanfall
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="chbox">
                                                        <input id="opt2" type="radio" name="opt" value="2" checked="">  
                                                        <label for="opt2"></label>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar color3" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
                                                                Battlefield 4
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="chbox">
                                                        <input id="opt3" type="radio" name="opt" value="3">  
                                                        <label for="opt3"></label>  
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </form>
                                    <p class="smal">Voting ends on 19th of October</p>
                                </div>
                            </div>

                            <!-- -->
                            <div class="sidebarblock">
                                <h3>My Active Threads</h3>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="#">This Dock Turns Your iPhone Into a Bedside Lamp</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="#">Who Wins in the Battle for Power on the Internet?</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="#">Sony QX10: A Funky, Overpriced Lens Camera for Your Smartphone</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="#">FedEx Simplifies Shipping for Small Businesses</a>
                                </div>
                                <div class="divline"></div>
                                <div class="blocktxt">
                                    <a href="#">Loud and Brave: Saudi Women Set to Protest Driving Ban</a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                                
                            
              
                <div class="container">
                    <div class="row">
                       <div class="col-lg-8 col-xs-12 col-md-8">
                             
                             <% if (sheet != 1) {%>
                            <div class="pull-left"><a href="forumDetails.jsp?page=<%=sheet - 1%>" class="prevnext"><i class="fa fa-angle-left"></i></a></div> 
                                <%} %> 
                            <div class="pull-left">
                                <ul class="paginationforum">
                            
                                    <% for (int i = 1; i <= DBO.getCountOfComments(); i++) {
                                    last2 += 1; %>
                                        <% if (sheet == i ) {%>
                                    <li><a class="active" href="forumDetails.jsp?page=<%=i%>&topic=<%fp.getId(); %>"><%=i%></a></li>
                                        <% } else {%>
                                    <li ><a class=""  href="forumDetails.jsp?page=<%=i %>&topic=<%fp.getId(); %>"><%=i%></a></li>
                                        <% }
                                            }%>                                                                                         
                                </ul>
                            </div>
                              <% if (last2-1 != sheet) {%>
                            <div class="pull-left"><a href="forumDetails.jsp?page=<%=sheet + 1%>&topic=<%fp.getId(); %>" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                            <% } %>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </section>


        </div>

        <!-- get jQuery from the google apis -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js"></script>

        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>


        <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
        <script type="text/javascript">

                  var revapi;

                  jQuery(document).ready(function() {
                      "use strict";
                      revapi = jQuery('.tp-banner').revolution(
                              {
                                  delay: 15000,
                                  startwidth: 1200,
                                  startheight: 278,
                                  hideThumbs: 10,
                                  fullWidth: "on"
                              });

                  });	//ready

        </script>

        <!-- END REVOLUTION SLIDER -->

    </body></html>
<% }%>