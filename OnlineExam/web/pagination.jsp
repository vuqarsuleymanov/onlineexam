<%@page import="az.mastersoft.javanshir.DAO"%>
<%@page import="az.mastersoft.javanshir.TopResultPojo"%>
<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <%
            int sheet = 1;
            int last = 1;
            if (request.getParameter("page") != null && Integer.parseInt(request.getParameter("page")) > 1) {
                sheet = Integer.parseInt(request.getParameter("page"));
            }

            int start = (sheet - 1) * 10;
        %>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Ana sehife</title>

        <!-- Core CSS - Include with every page -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Blank -->

        <!-- SB Admin CSS - Include with every page -->
        <link href="css/sb-admin.css" rel="stylesheet">
    </head>
    <%@include file="left_home.jsp"%>
    <body>

        <div id="wrapper">

            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">

                <%@include file="top_home.jsp"%>

            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blank</h1>
                    </div>
                    <form method="POST"><table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Ad</th>
                                    <th>Soyad</th>
                                    <th>Tarix</th>
                                    <th>Nəticə</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% for (TopResultPojo tp : DAO.topResult(start)) {%>
                                <tr>
                                    <td><%=tp.getFirstname()%></td> 
                                    <td><%=tp.getLastname()%></td>
                                    <td><%=tp.getTarix()%></td>
                                    <td><%=tp.getNetice()%></td>
                                </tr>
                                <%}%>
                            </tbody> 
                        </table>
                    </form>
                                 
                    <ul class="pagination pagination-lg">
                        <% if (sheet != 1) {%>
                        <li><a href="pagination.jsp?page=<%=sheet - 1%>">&laquo;</a></li>
                            <% } %>
                            <% if (sheet == 1) {%>
                        <li class="active"><a href="pagination.jsp?page=1">1</a></li>
                            <% } else { %>
                        <li ><a href="pagination.jsp?page=1">1</a></li>
                            <% } %>
                            <% for (int i = 1; i < DAO.getCountOfTopResult(); i++) {
                                    last += 1; %>
                            <% if (sheet == i + 1) {%>
                        <li class="active"><a href="pagination.jsp?page=<%=i + 1%>"><%=i + 1%></a></li>
                            <% } else {%>
                        <li ><a href="pagination.jsp?page=<%=i + 1%>"><%=i + 1%></a></li>
                            <% }
                                }
                                if (last != sheet) {%>
                        <li><a href="pagination.jsp?page=<%=sheet + 1%>">&raquo;</a></li>
                            <% }%>
                    </ul>
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Core Scripts - Include with every page -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Page-Level Plugin Scripts - Blank -->

        <!-- SB Admin Scripts - Include with every page -->
        <script src="js/sb-admin.js"></script>

        <!-- Page-Level Demo Scripts - Blank - Use for reference -->

    </body>

</html>
