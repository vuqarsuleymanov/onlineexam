<%@page import="az.mastersoft.rufan.Controller"%>
<%@page import="az.mastersoft.rufan.DBO"%>
<%@page import="az.mastersoft.rufan.UsersPojo"%>
<%@page import="az.mastersoft.natiq.MailSender"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="datepicker/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="datepicker/css/datepicker.css">
<link rel="stylesheet" href="files/css/bootstrap.css">

<script type="text/javascript">
   
    $(document).ready(function() {


         $("#zone").select2();
        $("#univercity").select2();
        $("#gender").select2();


        $('#birthday').datepicker({
            format: "dd/mm/yyyy"
        });

        $(".datepicker").css("z-index", 10000);

        $("#btnRegister").on("click", function() {

            $.ajax({
                url: "ajax/qeydiyyat.jsp",
                data: {
                    firstname: $("#firstname").val(),
                    lastname: $("#lastname").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    middlename: $("#middlename").val(),
                    password: $("#password").val(),
                    birthday: $("#birthday").val(),
                    gender: $("#gender").val(),
                    zone: $("#zone").val(),
                    univercity: $("#univercity").val()
                },
                success: function(data) {
                    var val = $(data).filter("#successfull");

                    if (val.text() === "successfull") {
                        window.location.href = "index.jsp?success";
                    } else if (val.text() === "dublicate") {
                        $("#register").html(data);
                    } else {

                        $("#register").html(data);

                    }
                }

            });

        });
    });
</script> 

<%
    if (!request.getParameter("firstname").trim().equals("")
            && !request.getParameter("lastname").trim().equals("")
            && !request.getParameter("email").trim().equals("")
            && !request.getParameter("password").trim().equals("")
            && !request.getParameter("phone").trim().equals("")
            && !request.getParameter("birthday").trim().equals("")
            && !request.getParameter("gender").trim().equals("")
            && !request.getParameter("zone").trim().equals("")
            && !request.getParameter("univercity").trim().equals("")) {

        UsersPojo up = new UsersPojo();

        up.setEmail(request.getParameter("email"));
        up.setFirstname(request.getParameter("firstname"));
        up.setLastname(request.getParameter("lastname"));
        up.setMiddlename(request.getParameter("middlename"));
        up.setPassword(request.getParameter("password"));
        up.setPhone(request.getParameter("phone"));
        up.setBirthDate(request.getParameter("birthday"));
        up.setGender(request.getParameter("gender"));
        up.setZone(request.getParameter("zone"));
        up.setUnivercity(request.getParameter("univercity"));
        up.setActive(0);
        up.setUsercode(DBO.GenerateUserCode());
        if (request.getParameter("gender").equals("K")) {
            up.setPhoto("img/male.jpg");
        } else if (request.getParameter("gender").equals("Q")) {
            up.setPhoto("img/famale.jpg");
        }
        if (!DBO.checkUser(up)) {

            DBO.insert(up);


%>
<span  id="successfull">successfull</span> 

<% } else {%>
<span style="display: none" id="successfull">dublicate</span> 
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <center><h4 class="modal-title">Qeydiyyat formu</h4></center>
</div>

<div class="modal-body">
    <div class="alert alert-danger" role="alert">Bu email artıq qeydiyyatdan keçib!<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
    <center><p>Şəxsi məlumatlarınızı daxil edin.</p></center><br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span style="width: 20px"><b style="font-family: Times New Roman; font-size: 21px">A</b><sup style="font-size: 15px;color: red;">*</sup></span></span>
        <input  type="text" class="form-control" id="firstname" placeholder="Adınız" name="firstname"  value="<%=request.getParameter("firstname") != null ? request.getParameter("firstname") : ""%>">

    </div> <br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span><b style="font-family: Times New Roman; font-size: 21px">S</b><sup style="font-size: 15px;color: red;">*</sup></span></span>
        <input type="text" class="form-control" id="lastname" placeholder="Soyadınız" name="lastname" value="<%=request.getParameter("lastname") != null ? request.getParameter("lastname") : ""%>">
    </div> <br/>
    <div class="input-group">
        <span class="input-group-addon"><b style="font-family: Times New Roman; font-size: 21px">A</b></span>
        <input type="text" class="form-control" id="middlename" placeholder="Ata adınız" name="middlename" value="<%=request.getParameter("middlename") != null ? request.getParameter("middlename") : ""%>">
    </div> <br/>
    <div class="input-group">
        <span style="font-size: 16px;"  class="input-group-addon"><span style="width: 20px" class="fa  fa-phone"><sup  style="color: red;">*</sup></span></span>
        <input type="text"  class="form-control" id="phone" placeholder="Mobil nömrəniz" name="phone" value="<%=request.getParameter("phone") != null ? request.getParameter("phone") : ""%>">
    </div> <br/> 

    <div class="input-group">
        <span style="font-size: 16px;"  class="input-group-addon"><span class="fa  fa-calendar"><sup  style=";color: red;">*</sup></span></span>
        <input type="text"  readonly="" class="form-control" style="cursor: pointer"  name="birthday" placeholder="Doğum gününüzü daxil edin"  id="birthday" value="<%=request.getParameter("birthday") %>">
    </div>

    <br>
    <div class="input-group">
        <span  class="input-group"><span ><b>Universitet</b></span></span>
        <select id="univercity" name="univercity" style="width: 570px;">

            <%    DBO d = new DBO();

                for (UsersPojo p : d.univercity()) {%>                                    
            <option> <%=p.getUnivercity()%></option>  
            <% } %>
        </select>


    </div> <br/> 
    <div class="input-group">
        <span  class="input-group"><span ><b>Yaşadığınız şəhər</b> </span></span>

        <select id="zone" name="zone" style="width: 570px;">


            <%
                for (UsersPojo p : d.zone()) {%>
            <option> <%=p.getZone()%></option>  

            <% }%>

        </select>

    </div><br>

    <div class="input-group"> 
        <span  class=" input-group "><span><b>Cinsiniz</b><sup style="font-size: 13px; color: red;">*</sup> </span></span>
        <select id="gender" name="gender" style="width: 570px;">
            <option value="K" selected="">Kişi</option>  
            <option value="Q">Qadın</option>  
        </select>

    </div><br> 
    <center><p>Hesab bilgilərinizi daxil edin.</p></center> <br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span class="fa  fa-envelope"><sup style=" ;color: red;">*</sup></span></span>
        <input type="email"  class="form-control" id="email" placeholder="Emailiniz" name="email" value="<%=request.getParameter("email") != null ? request.getParameter("email") : ""%>">
    </div> <br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span class="fa fa-lock"><sup style="color: red;">*</sup></span></span>
        <input type="password"  class="form-control" id="password" placeholder="Şifrə" name="password">
    </div>

</div>
<div class="modal-footer">
    <button id="btnRegister" onclick="modalSignForm()" class="btn btn-primary" name="registration">Qeydiyyat</button>
    <button  class="btn btn-default" data-dismiss="modal">Imtina</button>
</div>
<% }
} else {

%>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <center><h4 class="modal-title">Qeydiyyat formu</h4></center>
</div>

<div class="modal-body">
    <div class="alert alert-danger" role="alert">* olan hissələri doldurun! <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
    <center><p>Şəxsi məlumatlarınızı daxil edin.</p></center><br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span style="width: 20px"><b style="font-family: Times New Roman; font-size: 21px">A</b><sup style="font-size: 15px;color: red;">*</sup></span></span>
        <input  type="text" class="form-control" id="firstname" placeholder="Adınız" name="firstname"  value="<%=request.getParameter("firstname") != null ? request.getParameter("firstname") : ""%>">

    </div> <br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span><b style="font-family: Times New Roman; font-size: 21px">S</b><sup style="font-size: 15px;color: red;">*</sup></span></span>
        <input type="text" class="form-control" id="lastname" placeholder="Soyadınız" name="lastname" value="<%=request.getParameter("lastname") != null ? request.getParameter("lastname") : ""%>">
    </div> <br/>
    <div class="input-group">
        <span class="input-group-addon"><b style="font-family: Times New Roman; font-size: 21px">A</b></span>
        <input type="text" class="form-control" id="middlename" placeholder="Ata adınız" name="middlename" value="<%=request.getParameter("middlename") != null ? request.getParameter("middlename") : ""%>">
    </div> <br/>
    <div class="input-group">
        <span style="font-size: 16px;"  class="input-group-addon"><span style="width: 20px" class="fa  fa-phone"><sup  style="color: red;">*</sup></span></span>
        <input type="text"  class="form-control" id="phone" placeholder="Mobil nömrəniz" name="phone" value="<%=request.getParameter("phone") != null ? request.getParameter("phone") : ""%>">
    </div> <br/> 

    <div class="input-group">
        <span style="font-size: 16px;"  class="input-group-addon"><span class="fa  fa-calendar"><sup  style=";color: red;">*</sup></span></span>
        <input type="text"  readonly="" class="form-control" style="cursor: pointer"  name="birthday" placeholder="Doğum gününüzü daxil edin"  id="birthday"  value="<%=request.getParameter("birthday") %>">
    </div>

    <br>   <div class="input-group">
        <span  class="input-group"><span ><b>Universitet</b><sup style="font-size: 13px; color: red;">    </sup> </span></span>
        <select id="univercity" name="univercity" style="width: 570px;">

            <%    DBO d = new DBO();

                for (UsersPojo p : d.univercity()) {%>                                    
            <option> <%=p.getUnivercity()%></option>  
            <% } %>
        </select>


    </div> <br/> 
    <div class="input-group">
        <span  class="input-group"><span ><b>Yaşadığınız şəhər</b> </span></span>

        <select id="zone" name="zone" style="width: 570px;">


            <%
                for (UsersPojo p : d.zone()) {%>
            <option> <%=p.getZone()%></option>  

            <% }%>

        </select>

    </div><br>

    <div class="input-group"> 
        <span  class=" input-group "><span><b>Cinsiniz</b><sup style="font-size: 13px; color: red;"></sup> </span></span>
        <select id="gender" name="gender" style="width: 570px;">
            <option value="K" selected="">Kişi</option>  
            <option value="Q">Qadın</option>  
        </select>

    </div><br> 
    <center><p>Hesab bilgilərinizi daxil edin.</p></center> <br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span class="fa  fa-envelope"></span></span>
        <input type="email"  class="form-control" id="email" placeholder="Emailiniz" name="email" value="<%=request.getParameter("email") != null ? request.getParameter("email") : ""%>">
    </div> <br/>
    <div class="input-group">
        <span style="font-size: 16px;" class="input-group-addon"><span class="fa fa-lock"><sup style="color: red;">*</sup></span></span>
        <input type="password"  class="form-control" id="password" placeholder="Şifrə" name="password">
    </div>
    <div class="modal-footer">
        <button id="btnRegister" onclick="modalSignForm()" class="btn btn-primary" name="registration">Qeydiyyat</button>
        <button  class="btn btn-default" data-dismiss="modal">Imtina</button>
    </div>     
    <% }%>
