<%@page import="az.mastersoft.javanshir.QuestionPojo"%>
<%@page import="az.mastersoft.javanshir.DAO"%>
<%@page import="az.mastersoft.javanshir.TopResultPojo"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Ana sehife</title>

        <!-- Core CSS - Include with every page -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Blank -->

        <!-- SB Admin CSS - Include with every page -->
        <link href="css/sb-admin.css" rel="stylesheet">

    </head>
    <%@include file="left_home.jsp"%>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                <%@include file="top_home.jsp"%>
            </nav>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blank</h1>
                    </div>
                    <form method="POST"><table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Sual</th>
                                    <th>Cetinliyi</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <% for (QuestionPojo t : DAO.question_Find("mentiq")) {%> 
                                <tr>
                                    <td><%=t.getQuestion()%></td> 
                                    <td><%=t.getDifficulty_type()%></td>
                                    <td><%=t.getStatus()%></td>
                                </tr>
                                <% }%>
                            </tbody> 
                        </table>
                    </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- Core Scripts - Include with every page -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <!-- Page-Level Plugin Scripts - Blank -->
        <!-- SB Admin Scripts - Include with every page -->
        <script src="js/sb-admin.js"></script>
        <!-- Page-Level Demo Scripts - Blank - Use for reference -->

    </body>

</html>
