<%@page import="az.mastersoft.javanshir.DAO"%>
<%@page import="az.mastersoft.javanshir.TopResultPojo"%>
<%@page import="az.mastersoft.javanshir.TopResultPojo"%>
<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Ana sehife</title>

        <!-- Core CSS - Include with every page -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Blank -->
        <link href="css/jquery-ui.css" rel="stylesheet">
        <link href="css/jquery-ui.min.css" rel="stylesheet">

        <!-- SB Admin CSS - Include with every page -->
        <link href="css/sb-admin.css" rel="stylesheet">

    </head>
    <%@include file="left_home.jsp"%>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">

                <%@include file="top_home.jsp"%>

            </nav>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blank</h1>
                    </div>
                    <div class="btn-group btn-group-justified" style="padding-top: 10%;">
                        <div class="btn-group">
                            <a href="neticelerim.jsp">
                                <button type="button" class="btn btn-primary btn-lg" style="height: 332px">
                                    <br/><img src="http://icons.iconarchive.com/icons/iconleak/cerulean/256/chart-bar-chart-icon.png"
                                              alt=" " style="height: 60%; width: 65%; margin-bottom: 20px;">
                                    <br/><span class="glyphicon glyphicon-tasks"> Nəticələrim </span>
                                </button>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="start.jsp">
                                <button type="button" class="btn btn-success btn-lg" style="height: 332px">
                                    <br/><img src="http://icons.iconarchive.com/icons/chromatix/pulse-pack/256/start-icon.png"
                                              alt=" " style="height: 72%; width: 74%; margin-right: 22px;">
                                    <br/><span class="glyphicon glyphicon-off"> Imtahana başla </span>
                                </button>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="#">
                                <button type="button" class="btn btn-danger btn-lg" style="height: 332px">
                                    <br/><img src="http://icons.iconarchive.com/icons/7ur/iConPack/256/dowload-icon.png"
                                              alt=" " style="height: 70%; width: 74%;">
                                    <br/><span class="glyphicon glyphicon-share-alt"> Geri qayıt </span>
                                </button>
                            </a>
                        </div>
                    </div>

                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Core Scripts - Include with every page -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Page-Level Plugin Scripts - Blank -->

        <!-- SB Admin Scripts - Include with every page -->
        <script src="js/sb-admin.js"></script>

        <!-- Page-Level Demo Scripts - Blank - Use for reference -->
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery-ui.min.js"></script>

    </body>

</html>
