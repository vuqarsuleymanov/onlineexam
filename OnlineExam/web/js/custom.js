$(function() {
    $("#box").dialog({
        title: 'Xarici dil',
        modal: true,
        resizable: false,
        height: 250,
        buttons: [
            {
                text: 'Seç',
                click: function() {
                    changeOption();
                    $(this).dialog("close");
                }
            }
        ],
        dialogClass: 'no-close'
    });
});

