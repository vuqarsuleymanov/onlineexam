$(function() {
    $("#neticelerim").dialog({
        title: 'Nəticələrim',
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        },
        modal: true,
        resizable: false,
        width: 800,
        buttons: [
            {
                text: 'Bağla',
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
    $("#netice").click(function() {
        $("#neticelerim").dialog("open");
    });
});
