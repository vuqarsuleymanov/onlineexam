<%@page import="az.mastersoft.javanshir.TopResultPojo"%>
<%@page import="az.mastersoft.javanshir.DAO"%>
<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script type="text/javascript" src="engine1/jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="engine1/style.css" />
        <script type="text/javascript" src="engine1/jquery.js"></script>
        <!-- Core CSS - Include with every page -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Blank -->

        <!-- SB Admin CSS - Include with every page -->
        <link href="css/sb-admin.css" rel="stylesheet">

    </head>
    <body>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

    <%@include file="top_home.jsp" %>
    <%@include  file="left_home.jsp" %>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Blank</h1>
            </div>
        </div>
        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                    <li><a href="http://wowslider.com/vf"><img src="data1/images/11book_and_apples300x231.jpg" alt="full screen slider" title="11book_and_apples-300x231" id="wows1_0"/></a></li>
                    <li><img src="data1/images/bigstock_online_education_11092172.jpg" alt="bigstock_Online_Education_11092172" title="bigstock_Online_Education_11092172" id="wows1_1"/></li>
                </ul></div>
            <div class="ws_bullets"><div>
                    <a href="#" title="11book_and_apples-300x231"><img src="data1/tooltips/11book_and_apples300x231.jpg" alt="11book_and_apples-300x231"/>1</a>
                    <a href="#" title="bigstock_Online_Education_11092172"><img src="data1/tooltips/bigstock_online_education_11092172.jpg" alt="bigstock_Online_Education_11092172"/>2</a>
                </div></div><span class="wsl"><a href="http://wowslider.com/vh">slideshow html</a> by WOWSlider.com v6.6</span>
            <div class="ws_shadow"></div>
        </div>
        <form method="POST"><table class="table table-hover">
                <thead>
                    <tr>
                        <th>Ad</th>
                        <th>Soyad</th>
                        <th>Tarix</th>
                        <th>Nəticə</th>
                    </tr>
                </thead>
                <tbody> 
                    <% for (TopResultPojo t : DAO.topResult_Find(0, 140)) {%> 
                    <tr>
                        <td><%=t.getFirstname()%></td> 
                        <td><%=t.getLastname()%></td>
                        <td><%=t.getTarix()%></td>
                        <td><%=t.getNetice()%></td>
                    </tr>
                    <% }%>
                </tbody> 
            </table>
        </form>
        <script type="text/javascript" src="engine1/wowslider.js"></script>
        <script type="text/javascript" src="engine1/script.js"></script>
        <!-- /.row -->
    </div>
    <!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->


    <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->


    <!-- End WOWSlider.com BODY section -->
</body>
</html>

</html>
