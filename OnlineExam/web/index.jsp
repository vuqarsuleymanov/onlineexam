<%@page import="az.mastersoft.rufan.DBO"%>
<%@page import="az.mastersoft.rufan.UsersPojo"%>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <title>OnlineExam</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <meta charset="UTF-8" />
        
       
        <link rel="stylesheet" type="text/css" href="files/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="files/css/elastislide.css" />
        <link rel="stylesheet" type="text/css" href="files/css/custom.css" />
        <link rel="stylesheet" type="text/css" href="files/css/style.css" />
        <script src="files/js/modernizr.custom.17475.js"></script>
        <link href="files/css/bootstrap.css" rel="stylesheet"/>
        <script src="files/js/bootstrap.js"></script>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <script src="datepicker/js/bootstrap-datepicker.js"></script>
        <link rel="stylesheet" href="datepicker/css/datepicker.css">
        <link rel="stylesheet" href="files/css/bootstrap.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
        <link href="select2/select2.css" rel="stylesheet"/>
        <script src="select2/select2.js"></script>


        <script type="text/javascript">
            
            $(document).ready(function() {

                $('.open-modal').click(function() {
                    $('#myModal').modal('show');
                });
                $('.open-modal_signup').click(function() {
                    $('#SignUpModal').modal('show');
                });

                $("#zone").select2();
                $("#univercity").select2();
                $("#gender").select2();

                $('#birthday').datepicker({
                    format: "dd/mm/yyyy"
                });
                $(".datepicker").css("z-index", 10000);

                $("#btnRegister").on("click", function() {

                    $.ajax({
                        url: "ajax/qeydiyyat.jsp",
                        data: {
                            firstname: $("#firstname").val(),
                            lastname: $("#lastname").val(),
                            email: $("#email").val(),
                            phone: $("#phone").val(),
                            middlename: $("#middlename").val(),
                            password: $("#password").val(),
                            birthday: $("#birthday").val(),
                            gender: $("#gender").val(),
                            zone: $("#zone").val(),
                            univercity: $("#univercity").val()
                        },
                        success: function(data) {

                            var val = $(data).filter("#successfull");

                            if (val.text() === "successfull") {
                                window.location.href = "index.jsp?success";
                            } else if (val.text() === "dublicate") {
                                $("#register").html(data);
                            } else {

                                $("#register").html(data);

                            }
                        },
                        error: function(data) {

                        }

                    });

                });



               


            });


            function ValidateEmail(email) {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return expr.test(email);
            }
            ;

            function loginPanel() {
                var login_email = $('#login_email').val();
                var login_password = $('#login_password').val();

                if (login_email.length === 0 && login_password.length === 0) {
                    alert('Email və şifrənizi daxil edin.');
                    return false;
                }

                if (login_email.length === 0) {
                    alert('Emailinizi daxil edin.');
                    return false;
                }
                if (login_password.length === 0) {
                    alert('Şifrənizi daxil edin.');
                    return false;
                }

            }

        </script>


    </head>

    <body>


        <% if (request.getParameter("success") != null) {   %>
        <div style="height: 30px; vertical-align: middle !important; text-align: center" class=" alert-success" role="alert">
            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <b>Siz müvəffəqiyyətlə qeydiyyatdan keçdiniz</b>
        </div>
        <%}%>



        <%
            if (request.getParameter("error") != null) {
        %>

        <div style="height: 30px; vertical-align: middle !important; text-align: center" class=" alert-danger" role="alert">
            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <b>Email və ya şifrə düzgün deyil</b>

        </div> 

        <%
            }
        %> 
        <div class="container col-lg-12 col-md-12 col-xs-12 col-sm-12 " >

            <div class="panel panel-primary pull-right " style="margin-top: 10px; ">
                <div class="panel-heading" style="text-align: center">
                    <h4 style="font-family: Times New Roman" >Hesabınıza daxil olun.</h4>
                </div>
                <form action="UserDao">             
                    <div class="panel-body">

                        <div class="form-group">

                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                <input type="text" name="login_email" id="login_email" class="form-control input-sm" placeholder="Email"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="login_password" class="form-control input-sm" id="login_password" placeholder="Şifrə"/>
                            </div> 


                        </div>
                        <button class="btn btn-primary" name="btnLogin" onclick="loginPanel()" style="width: 100%">Daxil Ol</button>     
                        <br/>
                        <br/>
                        <div>

                            <a href="javascript: void()"  class="open-modal_signup">Qeydiyyat</a> 
                            <a href="javascript: void()" class="open-modal pull-right" >Şifrəni&nbsp;unutdum</a>
                        </div>
                    </div>
                </form>

                <div class="form-group">   
                </div> 

                <div id="myModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">  Şifrənin bərpa edilməsi</h4>
                            </div>
                            <div class="modal-body">
                                <p>Email adresinizi daxil edin.</p>

                                <form action="EmailSend" method="post">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                        <input type="text"  class="form-control" name="pass" id="inputPassword" placeholder="Emailiniz">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Göndər</button>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="SignUpModal" class="modal fade">
                    <div class="modal-dialog">

                        <div id="register" class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <center><h4 class="modal-title">Qeydiyyat formu</h4></center>
                            </div>

                            <div class="modal-body">
                                <center><p>Şəxsi məlumatlarınızı daxil edin.</p></center><br/>
                                <div class="input-group">
                                    <span  class="input-group-addon"><span><b style="font-family: Times New Roman; font-size: 21px">A</b><sup style="font-size: 15px;color: red;">*</sup></span></span>
                                    <input  type="text" class="form-control" id="firstname" placeholder="Adınız" name="firstname">
                                </div> <br/>
                                <div class="input-group">
                                    <span  class="input-group-addon"><span><b style="font-family: Times New Roman; font-size: 21px">S</b><sup style="font-size: 15px;color: red;">*</sup></span></span>
                                    <input type="text" class="form-control" id="lastname" placeholder="Soyadınız" name="lastname">
                                </div> <br/>
                                <div  class="input-group">
                                    <span class="input-group-addon"><b style="font-family: Times New Roman; font-size: 21px">A</b></span>
                                    <input type="text" class="form-control" id="middlename" placeholder="Ata adınız" name="middlename">
                                </div> <br/>
                                <div class="input-group">
                                    <span style="font-size: 16px;"  class="input-group-addon"><span style="width: 20px" class="fa  fa-phone"><sup  style="color: red;">*</sup></span></span>
                                    <input type="text"  class="form-control" id="phone" placeholder="Mobil nömrəniz" name="phone">
                                </div> <br/> 

                                <div class="input-group">
                                    <span style="font-size: 16px;"  class="input-group-addon"><span class="fa  fa-calendar"><sup  style=";color: red;">*</sup></span></span>
                                    <input type="text"  readonly="" class="form-control" style="cursor: pointer"  name="birthday" placeholder="Doğum gününüzü daxil edin"  id="birthday">
                                </div>

                                <br>
                                <div class="input-group">
                                    <span  class="input-group"><span ><b>Universitet</b><sup style="font-size: 13px; color: red;">    </sup> </span></span>
                                    <select id="univercity" name="univercity" style="width: 570px;">

                                        <%    DBO d = new DBO();

                                            for (UsersPojo p : d.univercity()) {%>                                    
                                        <option> <%=p.getUnivercity()%></option>  
                                        <% } %>
                                    </select>


                                </div> <br/> 
                                <div class="input-group">
                                    <span  class="input-group"><span ><b>Yaşadığınız şəhər</b> </span></span>

                                    <select id="zone" name="zone" style="width: 570px;">


                                        <%
                                            for (UsersPojo p : d.zone()) {%>
                                        <option> <%=p.getZone()%></option>  

                                        <% }%>

                                    </select>

                                </div><br>

                                <div class="input-group"> 
                                    <span  class=" input-group "><span><b>Cinsiniz</b></span></span>

                                    <select id="gender" name="gender" style="width: 570px;">
                                        <option value="K" selected="">Kişi</option>  
                                        <option value="Q">Qadın</option>  
                                    </select>

                                </div><br> 


                                <center><p>Hesab bilgilərinizi daxil edin.</p></center> <br/>
                                <div class="input-group">
                                    <span style="font-size: 16px;" class="input-group-addon"><span class="fa  fa-envelope"><sup style=" ;color: red;">*</sup></span></span>

                                    <input type="email"  class="form-control" id="email" placeholder="Emailiniz" name="email">
                                </div> <br/>
                                <div class="input-group">
                                    <span style="font-size: 16px;" class="input-group-addon"><span class="fa fa-lock"><sup style="color: red;">*</sup></span></span>
                                    <input type="password"  class="form-control" id="password" placeholder="Şifrə" name="password">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button id="btnRegister" onclick="modalSignForm()" class="btn btn-primary" name="registration">Qeydiyyat</button>
                                <button  class="btn btn-default" data-dismiss="modal">Imtina</button>
                            </div> 
                        </div>          

                    </div>
                </div>





            </div>






        </div>
        <div style="color: red"></div>
        <div class="navbar navbar-default navbar-fixed-bottom" style="text-align: center;background-color:#f8f8f8;border-color:#e7e7e7">

            <div class="fixed-bar">
                <h4>Əməkdaşlar</h4>

                <!-- Elastislide Carousel -->
                <ul id="carousel" class="elastislide-list">
                    <li><a href="#"><img src="files/images/small/1.jpg" alt="image01" /></a></li>
                    <li><a href="#"><img src="files/images/small/2.jpg" alt="image02" /></a></li>
                    <li><a href="#"><img src="files/images/small/3.jpg" alt="image03" /></a></li>
                    <li><a href="#"><img src="files/images/small/4.jpg" alt="image04" /></a></li>
                    <li><a href="#"><img src="files/images/small/5.jpg" alt="image05" /></a></li>
                    <li><a href="#"><img src="files/images/small/6.jpg" alt="image06" /></a></li>
                    <li><a href="#"><img src="files/images/small/7.jpg" alt="image07" /></a></li>
                    <li><a href="#"><img src="files/images/small/8.jpg" alt="image08" /></a></li>
                    <li><a href="#"><img src="files/images/small/9.jpg" alt="image09" /></a></li>
                    <li><a href="#"><img src="files/images/small/10.jpg" alt="image10" /></a></li>
                    <li><a href="#"><img src="files/images/small/11.jpg" alt="image11" /></a></li>
                    <li><a href="#"><img src="files/images/small/12.jpg" alt="image12" /></a></li>
                    <li><a href="#"><img src="files/images/small/13.jpg" alt="image13" /></a></li>
                    <li><a href="#"><img src="files/images/small/14.jpg" alt="image14" /></a></li>
                    <li><a href="#"><img src="files/images/small/15.jpg" alt="image15" /></a></li>
                    <li><a href="#"><img src="files/images/small/16.jpg" alt="image16" /></a></li>
                    <li><a href="#"><img src="files/images/small/17.jpg" alt="image17" /></a></li>
                    <li><a href="#"><img src="files/images/small/18.jpg" alt="image18" /></a></li>
                    <li><a href="#"><img src="files/images/small/19.jpg" alt="image19" /></a></li>
                    <li><a href="#"><img src="files/images/small/20.jpg" alt="image20" /></a></li>
                </ul>
                <!-- End Elastislide Carousel -->
            </div>

        </div>

        <script type="text/javascript" src="files/js/jquerypp.custom.js"></script>
        <script type="text/javascript" src="files/js/jquery.elastislide.js"></script>
        <script type="text/javascript">

                                    $('#carousel').elastislide();

        </script>

    </body>
</html>
